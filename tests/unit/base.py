import sys
import unittest
from unittest.mock import Mock, MagicMock

import warnings
warnings.simplefilter('ignore', Warning)


class BaseTest(unittest.TestCase):
    def setUp(self):
        __builtins__['_'] = lambda s: s
        self.mock = Mock()

    def tearDown(self):
        self.mock.UnsetStubs()
        self.mock.VerifyAll()


pygtk_mocks = ('Gtk', 'GObject')
pygtk_base_classes = ('GObject.GObject', 'Gtk.HBox', 'Gtk.Dialog')


class DummyBase(object):
    def __init__(self, *a, **k):
        # gtk.Dialog
        self.vbox = MagicMock()

    # gtk.Dialog
    def set_position(self, pos):
        pass


def mock_gtk():
    for module in pygtk_mocks:
        sys.modules[module] = MagicMock()

    for path in pygtk_base_classes:
        module, base_class = path.split('.')
        setattr(sys.modules[module], base_class, DummyBase)


def unmock_gtk():
    for module in pygtk_mocks:
        del sys.modules[module]
