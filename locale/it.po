# English translations for CHIRP package.
# Copyright (C) 2011 Dan Smith <dsmith@danplanet.com>
# This file is distributed under the same license as the CHIRP package.
# Dan Smith <dan@theine>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: CHIRP\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-20 12:29+0100\n"
"PO-Revision-Date: 2014-11-08 17:58+0100\n"
"Last-Translator: Dan Smith <dan@theine>\n"
"Language-Team: English\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.10\n"

#: ../chirp/ui/importdialog.py:96
#, python-brace-format
msgid ""
"Location {number} is already being imported. Choose another value for 'New "
"Location' before selection 'Import'"
msgstr ""
"La memoria numero {number} e' gia' stata importata. Scegliere un altro "
"valore per 'Nuova memoria' prima di selezionare 'Importa'"

#: ../chirp/ui/importdialog.py:128
msgid "Invalid value. Must be an integer."
msgstr "Valore non valido. Deve essere un numero intero"

#: ../chirp/ui/importdialog.py:137
#, python-brace-format
msgid "Location {number} is already being imported"
msgstr "La memoria {number} e' gia' stata importata"

#: ../chirp/ui/importdialog.py:194
msgid "Updating URCALL list"
msgstr "Aggiornamento lista URCALL"

#: ../chirp/ui/importdialog.py:199
msgid "Updating RPTCALL list"
msgstr "Aggiornamento lista RPTCALL"

#: ../chirp/ui/importdialog.py:272
#, python-brace-format
msgid "Setting memory {number}"
msgstr "Scrittura memoria {number}"

#: ../chirp/ui/importdialog.py:277
msgid "Importing bank information"
msgstr "Importazione informazioni bank"

#: ../chirp/ui/importdialog.py:281
msgid "Error importing memories:"
msgstr "Errore di importazione memorie:"

#: ../chirp/ui/importdialog.py:393
msgid "All"
msgstr "Tutto"

#: ../chirp/ui/importdialog.py:399
msgid "None"
msgstr "Nessuno"

#: ../chirp/ui/importdialog.py:405
msgid "Inverse"
msgstr "Inverti"

#: ../chirp/ui/importdialog.py:411
msgid "Select"
msgstr "Seleziona"

#: ../chirp/ui/importdialog.py:457
msgid "Auto"
msgstr "Auto"

#: ../chirp/ui/importdialog.py:463
msgid "Reverse"
msgstr "Rovescia"

#: ../chirp/ui/importdialog.py:469
msgid "Adjust New Location"
msgstr "Cambia nuova posizione"

#: ../chirp/ui/importdialog.py:479
msgid "Confirm overwrites"
msgstr "Conferma sovrascrittura"

#: ../chirp/ui/importdialog.py:485
msgid "Options"
msgstr "Opzioni"

#: ../chirp/ui/importdialog.py:558
msgid "Cannot be imported because"
msgstr "Non si puo' importare perche'"

#: ../chirp/ui/importdialog.py:575
msgid "Import From File"
msgstr "Importa da File"

#: ../chirp/ui/importdialog.py:576 ../chirp/ui/mainapp.py:1843
msgid "Import"
msgstr "Import"

#: ../chirp/ui/importdialog.py:596
msgid "To"
msgstr "A"

#: ../chirp/ui/importdialog.py:597
msgid "From"
msgstr "Da"

#: ../chirp/ui/importdialog.py:598 ../chirp/ui/memedit.py:79
#: ../chirp/ui/memedit.py:100 ../chirp/ui/memedit.py:351
#: ../chirp/ui/memedit.py:1127 ../chirp/ui/memedit.py:1186
#: ../chirp/ui/memedit.py:1339 ../chirp/ui/memedit.py:1341
#: ../chirp/ui/memdetail.py:337 ../chirp/ui/bankedit.py:93
#: ../chirp/ui/bankedit.py:283
msgid "Name"
msgstr "Nome"

#: ../chirp/ui/importdialog.py:599 ../chirp/ui/memedit.py:78
#: ../chirp/ui/memedit.py:101 ../chirp/ui/memedit.py:222
#: ../chirp/ui/memedit.py:344 ../chirp/ui/memedit.py:352
#: ../chirp/ui/memedit.py:381 ../chirp/ui/memedit.py:406
#: ../chirp/ui/memedit.py:414 ../chirp/ui/memedit.py:1128
#: ../chirp/ui/memedit.py:1183 ../chirp/ui/memdetail.py:335
#: ../chirp/ui/bankedit.py:282
msgid "Frequency"
msgstr "Frequenza"

#: ../chirp/ui/importdialog.py:600 ../chirp/ui/memedit.py:93
#: ../chirp/ui/memedit.py:115 ../chirp/ui/memedit.py:1144
#: ../chirp/ui/memedit.py:1201 ../chirp/ui/memedit.py:1344
#: ../chirp/ui/memdetail.py:365
msgid "Comment"
msgstr "Commento"

#: ../chirp/ui/importdialog.py:604
msgid "Location memory will be imported into"
msgstr "La memoria verra' importata in"

#: ../chirp/ui/importdialog.py:605
msgid "Location of memory in the file being imported"
msgstr "Posizione della memoria nel file che viene importato"

#: ../chirp/ui/importdialog.py:628
msgid "Preparing memory list..."
msgstr "Preparazione lista memorie..."

#: ../chirp/ui/importdialog.py:638
msgid "Export To File"
msgstr "Esporta in File"

#: ../chirp/ui/importdialog.py:639 ../chirp/ui/mainapp.py:1844
msgid "Export"
msgstr "Esporta"

#: ../chirp/ui/settingsedit.py:81
#, fuzzy, python-format
msgid "Error in setting value: %s"
msgstr "Errore di scrittura della memoria"

#: ../chirp/ui/settingsedit.py:113
#, python-format
msgid "Invalid setting value: %s"
msgstr ""

#: ../chirp/ui/settingsedit.py:174
msgid "Enabled"
msgstr ""

#: ../chirp/ui/shiftdialog.py:30
msgid "Shift"
msgstr "Shift"

#: ../chirp/ui/shiftdialog.py:64
#, python-brace-format
msgid "Moving {src} to {dst}"
msgstr "Spostamento da {src} a {dst}"

#: ../chirp/ui/shiftdialog.py:84
#, python-brace-format
msgid "Looking for a free spot ({number})"
msgstr "Ricerca di un posto libero ({number})"

#: ../chirp/ui/shiftdialog.py:97
msgid "No space to insert a row"
msgstr ""

#: ../chirp/ui/shiftdialog.py:145
#, python-brace-format
msgid "Moved {count} memories"
msgstr "Spostate {count} memorie"

#: ../chirp/ui/inputdialog.py:88
msgid "An error has occurred"
msgstr "Si e' verificato un errore"

#: ../chirp/ui/inputdialog.py:139
msgid "Overwrite"
msgstr "Sovrascrivi"

#: ../chirp/ui/inputdialog.py:142
msgid "File Exists"
msgstr "Il File esiste"

#: ../chirp/ui/inputdialog.py:145
#, python-brace-format
msgid "The file {name} already exists. Do you want to overwrite it?"
msgstr "Il file {name} esiste gia. Vuoi sovrascriverlo?"

#: ../chirp/ui/cloneprog.py:44
msgid "Clone Progress"
msgstr "Progressione programmazione"

#: ../chirp/ui/cloneprog.py:47
msgid "Cloning"
msgstr "Programmazione"

#: ../chirp/ui/cloneprog.py:56
msgid "Cancel"
msgstr "Annulla"

#: ../chirp/ui/memedit.py:64
msgid "Invalid value for this field"
msgstr "Valore non valido per questo campo"

#: ../chirp/ui/memedit.py:77 ../chirp/ui/memedit.py:207
#: ../chirp/ui/memedit.py:350 ../chirp/ui/memedit.py:425
#: ../chirp/ui/memedit.py:446 ../chirp/ui/memedit.py:460
#: ../chirp/ui/memedit.py:483 ../chirp/ui/memedit.py:505
#: ../chirp/ui/memedit.py:531 ../chirp/ui/memedit.py:543
#: ../chirp/ui/memedit.py:567 ../chirp/ui/memedit.py:569
#: ../chirp/ui/memedit.py:642 ../chirp/ui/memedit.py:656
#: ../chirp/ui/memedit.py:658 ../chirp/ui/memedit.py:700
#: ../chirp/ui/memedit.py:702 ../chirp/ui/memedit.py:776
#: ../chirp/ui/memedit.py:910 ../chirp/ui/memedit.py:921
#: ../chirp/ui/memedit.py:1007 ../chirp/ui/memedit.py:1062
#: ../chirp/ui/memedit.py:1125 ../chirp/ui/memedit.py:1153
#: ../chirp/ui/memedit.py:1166 ../chirp/ui/memedit.py:1184
#: ../chirp/ui/memedit.py:1522 ../chirp/ui/bankedit.py:281
msgid "Loc"
msgstr "Pos"

#: ../chirp/ui/memedit.py:80 ../chirp/ui/memedit.py:113
#: ../chirp/ui/memedit.py:128 ../chirp/ui/memedit.py:243
#: ../chirp/ui/memedit.py:249 ../chirp/ui/memedit.py:283
#: ../chirp/ui/memedit.py:422 ../chirp/ui/memedit.py:1129
#: ../chirp/ui/memedit.py:1192 ../chirp/ui/memedit.py:1345
#: ../chirp/ui/memedit.py:1405 ../chirp/ui/memdetail.py:339
msgid "Tone Mode"
msgstr "Modalita' tono"

#: ../chirp/ui/memedit.py:81 ../chirp/ui/memedit.py:102
#: ../chirp/ui/memedit.py:119 ../chirp/ui/memedit.py:178
#: ../chirp/ui/memedit.py:179 ../chirp/ui/memedit.py:268
#: ../chirp/ui/memedit.py:298 ../chirp/ui/memedit.py:305
#: ../chirp/ui/memedit.py:310 ../chirp/ui/memedit.py:318
#: ../chirp/ui/memedit.py:355 ../chirp/ui/memedit.py:420
#: ../chirp/ui/memedit.py:1130 ../chirp/ui/memedit.py:1188
#: ../chirp/ui/memedit.py:1346 ../chirp/ui/memdetail.py:341
msgid "Tone"
msgstr "Tone"

#: ../chirp/ui/memedit.py:82 ../chirp/ui/memedit.py:103
#: ../chirp/ui/memedit.py:120 ../chirp/ui/memedit.py:272
#: ../chirp/ui/memedit.py:291 ../chirp/ui/memedit.py:306
#: ../chirp/ui/memedit.py:311 ../chirp/ui/memedit.py:322
#: ../chirp/ui/memedit.py:356 ../chirp/ui/memedit.py:420
#: ../chirp/ui/memedit.py:1131 ../chirp/ui/memedit.py:1189
#: ../chirp/ui/memedit.py:1342 ../chirp/ui/memdetail.py:343
msgid "ToneSql"
msgstr "ToneSql"

#: ../chirp/ui/memedit.py:83 ../chirp/ui/memedit.py:104
#: ../chirp/ui/memedit.py:121 ../chirp/ui/memedit.py:256
#: ../chirp/ui/memedit.py:292 ../chirp/ui/memedit.py:300
#: ../chirp/ui/memedit.py:312 ../chirp/ui/memedit.py:320
#: ../chirp/ui/memedit.py:357 ../chirp/ui/memedit.py:416
#: ../chirp/ui/memedit.py:1132 ../chirp/ui/memedit.py:1190
#: ../chirp/ui/memedit.py:1334 ../chirp/ui/memedit.py:1411
#: ../chirp/ui/memdetail.py:345
msgid "DTCS Code"
msgstr "DTCS Code"

#: ../chirp/ui/memedit.py:84 ../chirp/ui/memedit.py:105
#: ../chirp/ui/memedit.py:122 ../chirp/ui/memedit.py:260
#: ../chirp/ui/memedit.py:293 ../chirp/ui/memedit.py:301
#: ../chirp/ui/memedit.py:308 ../chirp/ui/memedit.py:313
#: ../chirp/ui/memedit.py:324 ../chirp/ui/memedit.py:358
#: ../chirp/ui/memedit.py:416 ../chirp/ui/memedit.py:1133
#: ../chirp/ui/memedit.py:1191 ../chirp/ui/memedit.py:1335
#: ../chirp/ui/memedit.py:1412
#, fuzzy
msgid "DTCS Rx Code"
msgstr "DTCS Code"

#: ../chirp/ui/memedit.py:85 ../chirp/ui/memedit.py:106
#: ../chirp/ui/memedit.py:123 ../chirp/ui/memedit.py:264
#: ../chirp/ui/memedit.py:294 ../chirp/ui/memedit.py:302
#: ../chirp/ui/memedit.py:314 ../chirp/ui/memedit.py:326
#: ../chirp/ui/memedit.py:359 ../chirp/ui/memedit.py:1134
#: ../chirp/ui/memedit.py:1194 ../chirp/ui/memedit.py:1336
#: ../chirp/ui/memedit.py:1410 ../chirp/ui/memdetail.py:349
msgid "DTCS Pol"
msgstr "DTCS Pol"

#: ../chirp/ui/memedit.py:86 ../chirp/ui/memedit.py:107
#: ../chirp/ui/memedit.py:129 ../chirp/ui/memedit.py:244
#: ../chirp/ui/memedit.py:254 ../chirp/ui/memedit.py:276
#: ../chirp/ui/memedit.py:285 ../chirp/ui/memedit.py:295
#: ../chirp/ui/memedit.py:303 ../chirp/ui/memedit.py:307
#: ../chirp/ui/memedit.py:315 ../chirp/ui/memedit.py:360
#: ../chirp/ui/memedit.py:1135 ../chirp/ui/memedit.py:1193
#: ../chirp/ui/memedit.py:1343 ../chirp/ui/memedit.py:1406
msgid "Cross Mode"
msgstr "Cross Mode"

#: ../chirp/ui/memedit.py:87 ../chirp/ui/memedit.py:108
#: ../chirp/ui/memedit.py:126 ../chirp/ui/memedit.py:152
#: ../chirp/ui/memedit.py:159 ../chirp/ui/memedit.py:284
#: ../chirp/ui/memedit.py:353 ../chirp/ui/memedit.py:422
#: ../chirp/ui/memedit.py:1136 ../chirp/ui/memedit.py:1195
#: ../chirp/ui/memedit.py:1347 ../chirp/ui/memedit.py:1419
#: ../chirp/ui/memdetail.py:353
msgid "Duplex"
msgstr "Duplex"

#: ../chirp/ui/memedit.py:88 ../chirp/ui/memedit.py:109
#: ../chirp/ui/memedit.py:157 ../chirp/ui/memedit.py:230
#: ../chirp/ui/memedit.py:329 ../chirp/ui/memedit.py:354
#: ../chirp/ui/memedit.py:418 ../chirp/ui/memedit.py:1137
#: ../chirp/ui/memedit.py:1196 ../chirp/ui/memedit.py:1338
#: ../chirp/ui/memdetail.py:355
msgid "Offset"
msgstr "Offset"

#: ../chirp/ui/memedit.py:89 ../chirp/ui/memedit.py:110
#: ../chirp/ui/memedit.py:124 ../chirp/ui/memedit.py:171
#: ../chirp/ui/memedit.py:172 ../chirp/ui/memedit.py:175
#: ../chirp/ui/memedit.py:406 ../chirp/ui/memedit.py:1138
#: ../chirp/ui/memedit.py:1197 ../chirp/ui/memedit.py:1337
#: ../chirp/ui/memedit.py:1404 ../chirp/ui/memedit.py:1421
#: ../chirp/ui/memedit.py:1422 ../chirp/ui/memedit.py:1605
#: ../chirp/ui/memedit.py:1623 ../chirp/ui/memedit.py:1633
#: ../chirp/ui/memdetail.py:357
msgid "Mode"
msgstr "Modulazione"

#: ../chirp/ui/memedit.py:90 ../chirp/ui/memedit.py:111
#: ../chirp/ui/memedit.py:125 ../chirp/ui/memedit.py:406
#: ../chirp/ui/memedit.py:1139 ../chirp/ui/memedit.py:1198
#: ../chirp/ui/memedit.py:1349 ../chirp/ui/memedit.py:1408
#: ../chirp/ui/memedit.py:1415 ../chirp/ui/memdetail.py:363
msgid "Power"
msgstr "Potenza"

#: ../chirp/ui/memedit.py:91 ../chirp/ui/memedit.py:112
#: ../chirp/ui/memedit.py:127 ../chirp/ui/memedit.py:162
#: ../chirp/ui/memedit.py:163 ../chirp/ui/memedit.py:168
#: ../chirp/ui/memedit.py:1142 ../chirp/ui/memedit.py:1199
#: ../chirp/ui/memedit.py:1340 ../chirp/ui/memedit.py:1417
#: ../chirp/ui/memdetail.py:359
msgid "Tune Step"
msgstr "Step sintonia"

#: ../chirp/ui/memedit.py:92 ../chirp/ui/memedit.py:114
#: ../chirp/ui/memedit.py:1143 ../chirp/ui/memedit.py:1200
#: ../chirp/ui/memedit.py:1348 ../chirp/ui/memedit.py:1407
#: ../chirp/ui/memdetail.py:361
msgid "Skip"
msgstr "Salta"

#: ../chirp/ui/memedit.py:210
#, python-brace-format
msgid "Erasing memory {loc}"
msgstr "Cancellazione memoria {loc}"

#: ../chirp/ui/memedit.py:339
msgid "Unable to make changes to this model"
msgstr "Impossibile effettuare modifiche su questo modello"

#: ../chirp/ui/memedit.py:345
msgid "Editing new item, taking defaults"
msgstr "Modifica di un nuovo elemento, verranno usati valori di default"

#: ../chirp/ui/memedit.py:367
#, python-brace-format
msgid "Bad value for {col}: {val}"
msgstr "Valore non valido per {col}: {val}"

#: ../chirp/ui/memedit.py:391
msgid "Error setting memory"
msgstr "Errore di scrittura della memoria"

#: ../chirp/ui/memedit.py:399 ../chirp/ui/memedit.py:467
#: ../chirp/ui/memedit.py:743 ../chirp/ui/memedit.py:766
#: ../chirp/ui/memedit.py:1575
#, python-brace-format
msgid "Writing memory {number}"
msgstr "Scrittura memoria {number}"

#: ../chirp/ui/memedit.py:472
msgid ""
"This operation requires moving all subsequent channels by one spot until an "
"empty location is reached.  This can take a LONG time.  Are you sure you "
"want to do this?"
msgstr ""
"Questa operazione richiede lo spostamento dei canali seguenti di un passo "
"alla volta fino alla nuova posizione. L'operazione puo' durare MOLTO tempo. "
"Sei sicuro di volerlo fare?"

#: ../chirp/ui/memedit.py:496
#, python-brace-format
msgid "Adding memory {number}"
msgstr "Aggiunta memoria {number}"

#: ../chirp/ui/memedit.py:509 ../chirp/ui/memedit.py:1173
#, python-brace-format
msgid "Erasing memory {number}"
msgstr "Cancellatura memoria {number}"

#: ../chirp/ui/memedit.py:518 ../chirp/ui/memedit.py:626
#: ../chirp/ui/memedit.py:673 ../chirp/ui/memedit.py:678
#: ../chirp/ui/memedit.py:1111 ../chirp/ui/memedit.py:1437
#, python-brace-format
msgid "Getting memory {number}"
msgstr "Acquisita memoria {number}"

#: ../chirp/ui/memedit.py:605 ../chirp/ui/memedit.py:616
#: ../chirp/ui/memedit.py:665
#, python-brace-format
msgid "Moving memory from {old} to {new}"
msgstr "Spostata memoria da {old} a {new}"

#: ../chirp/ui/memedit.py:687
#, python-brace-format
msgid "Raw memory {number}"
msgstr "Memoria Raw {number}"

#: ../chirp/ui/memedit.py:691 ../chirp/ui/memedit.py:719
#: ../chirp/ui/memedit.py:724
#, python-brace-format
msgid "Getting raw memory {number}"
msgstr "Acquisizione memoria Raw {number}"

#: ../chirp/ui/memedit.py:696
msgid "You can only diff two memories!"
msgstr "Puoi differenziare solo 2 memorie!"

#: ../chirp/ui/memedit.py:707
#, python-brace-format
msgid "Memory {number}"
msgstr "Memoria {number}"

#: ../chirp/ui/memedit.py:713
#, python-brace-format
msgid "Diff of {a} and {b}"
msgstr "Differenza di {a} e {b}"

#: ../chirp/ui/memedit.py:747
#, fuzzy, python-brace-format
msgid "Getting original memory {number}"
msgstr "Acquisizione memoria Raw {number}"

#: ../chirp/ui/memedit.py:785
msgid "Memories must be contiguous"
msgstr "Le memorie devono essere contigue"

#: ../chirp/ui/memedit.py:876
msgid "Cut"
msgstr "Taglia"

#: ../chirp/ui/memedit.py:877
msgid "Copy"
msgstr "Copia"

#: ../chirp/ui/memedit.py:878
msgid "Paste"
msgstr "Incolla"

#: ../chirp/ui/memedit.py:879
#, fuzzy
msgid "Select All"
msgstr "Seleziona"

#: ../chirp/ui/memedit.py:880
msgid "Insert row above"
msgstr "Inserisci riga sopra"

#: ../chirp/ui/memedit.py:881
msgid "Insert row below"
msgstr "Inserisci riga sotto"

#: ../chirp/ui/memedit.py:882
msgid "Delete"
msgstr "Elimina"

#: ../chirp/ui/memedit.py:883
#, fuzzy
msgid "this memory"
msgstr "Preparazione memoria"

#: ../chirp/ui/memedit.py:883
#, fuzzy
msgid "these memories"
msgstr "Scambia memorie"

#: ../chirp/ui/memedit.py:884
msgid "...and shift block up"
msgstr ""

#: ../chirp/ui/memedit.py:885
msgid "...and shift all memories up"
msgstr ""

#: ../chirp/ui/memedit.py:886
msgid "Move up"
msgstr "Sposta su"

#: ../chirp/ui/memedit.py:887
msgid "Move down"
msgstr "Muovi giu"

#: ../chirp/ui/memedit.py:888
msgid "Exchange memories"
msgstr "Scambia memorie"

#: ../chirp/ui/memedit.py:889 ../chirp/ui/mainapp.py:1827
msgid "P_roperties"
msgstr ""

#: ../chirp/ui/memedit.py:890
msgid "Show Raw Memory"
msgstr "Mostra memorie Raw"

#: ../chirp/ui/memedit.py:891
msgid "Diff Raw Memories"
msgstr "Diff memorie Raw"

#: ../chirp/ui/memedit.py:923 ../chirp/ui/memedit.py:942
msgid "_filled"
msgstr ""

#: ../chirp/ui/memedit.py:1079
#, python-brace-format
msgid "Internal Error: Column {name} not found"
msgstr "Errore interno: nome colonna {name} non trovato"

#: ../chirp/ui/memedit.py:1088
#, fuzzy, python-format
msgid "Internal Error: Renderer for column %s not found"
msgstr "Errore interno: nome colonna {name} non trovato"

#: ../chirp/ui/memedit.py:1118
#, python-brace-format
msgid "Getting channel {chan}"
msgstr "Acquisizione canale {chan}"

#: ../chirp/ui/memedit.py:1213
#, python-brace-format
msgid "Internal Error: Invalid limit {number}"
msgstr "Errore interno: limite non valido {number}"

#: ../chirp/ui/memedit.py:1224
#, fuzzy
msgid "Memory Range:"
msgstr "Range memorie"

#: ../chirp/ui/memedit.py:1253
msgid "Refresh"
msgstr ""

#: ../chirp/ui/memedit.py:1276
msgid "Special Channels"
msgstr "Canali Speciali"

#: ../chirp/ui/memedit.py:1284
msgid "Show Empty"
msgstr "Mostra Vuoti"

#: ../chirp/ui/memedit.py:1296
msgid "Properties"
msgstr ""

#: ../chirp/ui/memedit.py:1471
#, python-brace-format
msgid "Cutting memory {number}"
msgstr "Taglia memoria {number}"

#: ../chirp/ui/memedit.py:1510
#, python-brace-format
msgid ""
"Unable to paste {src} memories into {dst} rows. Increase the memory bounds "
"or show empty memories."
msgstr ""

#: ../chirp/ui/memedit.py:1524
msgid "Overwrite?"
msgstr "Sovrascrivere?"

#: ../chirp/ui/memedit.py:1530
#, python-brace-format
msgid "Overwrite location {number}?"
msgstr "Sovrascrivere posizione {number}?"

#: ../chirp/ui/memedit.py:1555
msgid "Incompatible Memory"
msgstr "Memoria non compatibile"

#: ../chirp/ui/memedit.py:1559
#, python-brace-format
msgid "Pasted memory {number} is not compatible with this radio because:"
msgstr ""
"La memoria incollata {number} non e' compatibile con questa radio perche':"

#: ../chirp/ui/memedit.py:1625 ../chirp/ui/memedit.py:1640
msgid "URCALL"
msgstr "URCALL"

#: ../chirp/ui/memedit.py:1625 ../chirp/ui/memedit.py:1641
msgid "RPT1CALL"
msgstr "RPT1CALL"

#: ../chirp/ui/memedit.py:1625 ../chirp/ui/memedit.py:1642
msgid "RPT2CALL"
msgstr "RPT2CALL"

#: ../chirp/ui/memedit.py:1626 ../chirp/ui/memedit.py:1643
msgid "Digital Code"
msgstr "Digital Code"

#: ../chirp/ui/memedit.py:1681 ../chirp/ui/dstaredit.py:177
msgid "Downloading URCALL list"
msgstr "Scaricamento lista URCALL"

#: ../chirp/ui/memedit.py:1693 ../chirp/ui/dstaredit.py:181
msgid "Downloading RPTCALL list"
msgstr "Scaricamento lista RPTCALL"

#: ../chirp/ui/mainapp.py:268 ../chirp/ui/mainapp.py:503
msgid "Untitled"
msgstr "Senza Nome"

#: ../chirp/ui/mainapp.py:319 ../chirp/ui/mainapp.py:840
msgid "All files"
msgstr ""

#: ../chirp/ui/mainapp.py:320 ../chirp/ui/mainapp.py:842
msgid "CHIRP Radio Images"
msgstr "Immagine Radio CHIRP"

#: ../chirp/ui/mainapp.py:321 ../chirp/ui/mainapp.py:841
msgid "CHIRP Files"
msgstr "Files CHIRP"

#: ../chirp/ui/mainapp.py:322 ../chirp/ui/mainapp.py:843
#: ../chirp/ui/mainapp.py:1402
msgid "CSV Files"
msgstr "Files CSV"

#: ../chirp/ui/mainapp.py:323 ../chirp/ui/mainapp.py:844
#, fuzzy
msgid "DAT Files"
msgstr "Files CSV"

#: ../chirp/ui/mainapp.py:324 ../chirp/ui/mainapp.py:845
msgid "EVE Files (VX5)"
msgstr ""

#: ../chirp/ui/mainapp.py:325 ../chirp/ui/mainapp.py:846
msgid "ICF Files"
msgstr "Files ICF"

#: ../chirp/ui/mainapp.py:326 ../chirp/ui/mainapp.py:850
#, fuzzy
msgid "VX5 Commander Files"
msgstr "Files VX7 Commander"

#: ../chirp/ui/mainapp.py:327 ../chirp/ui/mainapp.py:851
#, fuzzy
msgid "VX6 Commander Files"
msgstr "Files VX7 Commander"

#: ../chirp/ui/mainapp.py:328 ../chirp/ui/mainapp.py:852
msgid "VX7 Commander Files"
msgstr "Files VX7 Commander"

#: ../chirp/ui/mainapp.py:338
msgid ""
"ICF files cannot be edited, only displayed or imported into another file. "
"Open in read-only mode?"
msgstr ""
"I Files ICF non possono essere modificati, solo visualizzati o importati in "
"un altro file. Aprire in sola lettura?"

#: ../chirp/ui/mainapp.py:361
msgid ""
"Unable to open this image. It was generated with a newer version of CHIRP "
"and thus may be for a radio model that is not supported by this version. "
"Please update to the latest version of CHIRP and try again."
msgstr ""

#: ../chirp/ui/mainapp.py:370
#, fuzzy
msgid "Unable to open this image: unsupported model"
msgstr "Impossibile effettuare modifiche su questo modello"

#: ../chirp/ui/mainapp.py:390
#, python-brace-format
msgid "There was an error opening {fname}: {error}"
msgstr "Errore durante l'apertura di {fname}: {error}"

#: ../chirp/ui/mainapp.py:402
#, python-brace-format
msgid "{num} errors during open:"
msgstr "{num} errori durante l'apertura:"

#: ../chirp/ui/mainapp.py:410
msgid "Note:"
msgstr "Note:"

#: ../chirp/ui/mainapp.py:411
#, python-brace-format
msgid ""
"The {vendor} {model} operates in <b>live mode</b>. This means that any "
"changes you make are immediately sent to the radio. Because of this, you "
"cannot perform the <u>Save</u> or <u>Upload</u> operations. If you wish to "
"edit the contents offline, please <u>Export</u> to a CSV file, using the "
"<b>File menu</b>."
msgstr ""
"La radio{vendor} {model} opera in <b>live mode</b>. Questo significa che "
"qualsiasi modifica viene immediatamente scritta nella radio. A causa di "
"questo, non puoi utilizzare le funzioni  <u>Salva</u> or <u>Scrivi</u>. Se "
"vuoi modificare le memorie offline, per favore <u>Esporta</u> le memorie in "
"un file CSV, usando il menu <b>File</b>."

#: ../chirp/ui/mainapp.py:420
msgid "Don't show this again"
msgstr "Non mostrarlo di nuovo"

#: ../chirp/ui/mainapp.py:454
#, python-brace-format
msgid "{vendor} {model} image file"
msgstr "File immagine {vendor} {model}"

#: ../chirp/ui/mainapp.py:470
msgid "VX7 Commander"
msgstr "VX7 Commander"

#: ../chirp/ui/mainapp.py:472
#, fuzzy
msgid "VX6 Commander"
msgstr "VX7 Commander"

#: ../chirp/ui/mainapp.py:474
msgid "EVE"
msgstr ""

#: ../chirp/ui/mainapp.py:475
#, fuzzy
msgid "VX5 Commander"
msgstr "VX7 Commander"

#: ../chirp/ui/mainapp.py:542
#, python-brace-format
msgid "Open recent file {name}"
msgstr "Apri file recente {name}"

#: ../chirp/ui/mainapp.py:599
#, python-brace-format
msgid "Import stock configuration {name}"
msgstr "Importazione configurazione stock {name}"

#: ../chirp/ui/mainapp.py:615
#, python-brace-format
msgid "Open stock configuration {name}"
msgstr "Apertura configurazione stock {name}"

#: ../chirp/ui/mainapp.py:636
msgid "Proceed with experimental driver?"
msgstr ""

#: ../chirp/ui/mainapp.py:638
msgid "This radio's driver is experimental. Do you want to proceed?"
msgstr ""

#: ../chirp/ui/mainapp.py:657
#, fuzzy, python-brace-format
msgid "{name} Information"
msgstr "Acquisizione informazioni bank"

#: ../chirp/ui/mainapp.py:659
#, python-brace-format
msgid "{information}"
msgstr ""

#: ../chirp/ui/mainapp.py:663
#, fuzzy
msgid "Don't show information for any radio again"
msgstr "Non mostrarlo di nuovo"

#: ../chirp/ui/mainapp.py:687
#, python-brace-format
msgid "{name} Instructions"
msgstr ""

#: ../chirp/ui/mainapp.py:689
#, python-brace-format
msgid "{instructions}"
msgstr ""

#: ../chirp/ui/mainapp.py:693
#, fuzzy
msgid "Don't show instructions for any radio again"
msgstr "Non mostrarlo di nuovo"

#: ../chirp/ui/mainapp.py:807
#, fuzzy
msgid "Save Changes?"
msgstr "Scartare le modifiche?"

#: ../chirp/ui/mainapp.py:811
msgid "File is modified, save changes before closing?"
msgstr "Il file e' stato modificato, salvare i cambiamenti prima di chiudere?"

#: ../chirp/ui/mainapp.py:847
msgid "Kenwood HMK Files"
msgstr ""

#: ../chirp/ui/mainapp.py:848
msgid "Kenwood ITM Files"
msgstr ""

#: ../chirp/ui/mainapp.py:849
msgid "Travel Plus Files"
msgstr ""

#: ../chirp/ui/mainapp.py:867
msgid "DMR-MARC Repeater Database Dump"
msgstr ""

#: ../chirp/ui/mainapp.py:977 ../chirp/ui/mainapp.py:1089
msgid "RepeaterBook Query"
msgstr ""

#: ../chirp/ui/mainapp.py:1043 ../chirp/ui/mainapp.py:1155
msgid "RepeaterBook query failed"
msgstr ""

#: ../chirp/ui/mainapp.py:1222
#, fuzzy, python-format
msgid "Invalid value for %s"
msgstr "Valore non valido per questo campo"

#: ../chirp/ui/mainapp.py:1247
msgid "Query failed"
msgstr ""

#: ../chirp/ui/mainapp.py:1342
msgid "RadioReference.com Query"
msgstr ""

#: ../chirp/ui/mainapp.py:1476
msgid "Select Columns"
msgstr "Seleziona colonne"

#: ../chirp/ui/mainapp.py:1491
#, python-brace-format
msgid "Visible columns for {radio}"
msgstr "Colonne visibili per {radio}"

#: ../chirp/ui/mainapp.py:1554
msgid "Reporting is disabled"
msgstr "Reporting disattivato"

#: ../chirp/ui/mainapp.py:1556
msgid ""
"The reporting feature of CHIRP is designed to help <u>improve quality</u> by "
"allowing the authors to focus on the radio drivers used most often and "
"errors experienced by the users. The reports contain no identifying "
"information and are used only for statistical purposes by the authors. Your "
"privacy is extremely important, but <u>please consider leaving this feature "
"enabled to help make CHIRP better!</u>\n"
"\n"
"<b>Are you sure you want to disable this feature?</b>"
msgstr ""
"La funzione reporting di CHIRP e' fatta per <u>migliorare la qualita'</u> "
"consentendo agli autori di concentrarsi sulle radio piu' utilizzate e sugli "
"errori piu' comuni. I report non contengono informazioni personali e sono "
"utilizzati per soli fini statistici dagli autori. La tua provacy e' molto "
"importante ma <u>per favore considera di lasciare attiva questa opzione per "
"aiutarci a migliorare CHIRP!</u>\n"
"\n"
"<b>Sei sicuro di disabilitare questa opzione?</b>"

#: ../chirp/ui/mainapp.py:1599
msgid ""
"Choose a language or Auto to use the operating system default. You will need "
"to restart the application before the change will take effect"
msgstr ""
"Scegli una lingua o usa Auto per utilizzare la lingua del sistema operativo. "
"Devi riavviare l'applicazione per applicare i cambiamenti"

#: ../chirp/ui/mainapp.py:1612
#, fuzzy
msgid "Python Modules"
msgstr "Modalita' tono"

#: ../chirp/ui/mainapp.py:1613
msgid "Modules"
msgstr ""

#: ../chirp/ui/mainapp.py:1804
msgid "_File"
msgstr "File"

#: ../chirp/ui/mainapp.py:1807
msgid "Open stock config"
msgstr "Apri configurazione stock"

#: ../chirp/ui/mainapp.py:1808
msgid "_Recent"
msgstr "Recenti"

#: ../chirp/ui/mainapp.py:1811
#, fuzzy
msgid "Load Module"
msgstr "Modalita' tono"

#: ../chirp/ui/mainapp.py:1814
msgid "_Edit"
msgstr "Modifica"

#: ../chirp/ui/mainapp.py:1815
msgid "_Cut"
msgstr "Taglia"

#: ../chirp/ui/mainapp.py:1816
msgid "_Copy"
msgstr "Copia"

#: ../chirp/ui/mainapp.py:1817
msgid "_Paste"
msgstr "Incolla"

#: ../chirp/ui/mainapp.py:1819
msgid "_Delete"
msgstr "Elimina"

#: ../chirp/ui/mainapp.py:1820
#, fuzzy
msgid "Select _All"
msgstr "Seleziona"

#: ../chirp/ui/mainapp.py:1821
msgid "Move _Up"
msgstr "Muovi Su"

#: ../chirp/ui/mainapp.py:1823
msgid "Move Dow_n"
msgstr "Muovi Giu"

#: ../chirp/ui/mainapp.py:1825
msgid "E_xchange"
msgstr "Scambia"

#: ../chirp/ui/mainapp.py:1828
msgid "_View"
msgstr "Vista"

#: ../chirp/ui/mainapp.py:1829
msgid "Columns"
msgstr "Colonne"

#: ../chirp/ui/mainapp.py:1830
msgid "Developer"
msgstr "Sviluppatore"

#: ../chirp/ui/mainapp.py:1831
msgid "Show raw memory"
msgstr "Mostra memorie Raw"

#: ../chirp/ui/mainapp.py:1833
msgid "Diff raw memories"
msgstr "Differenza memorie Raw"

#: ../chirp/ui/mainapp.py:1835
msgid "Diff tabs"
msgstr "Differenzia tabs"

#: ../chirp/ui/mainapp.py:1837
msgid "Change language"
msgstr "Cambia lingua"

#: ../chirp/ui/mainapp.py:1838
msgid "_Radio"
msgstr "Radio"

#: ../chirp/ui/mainapp.py:1839
msgid "Download From Radio"
msgstr "Leggi da Radio"

#: ../chirp/ui/mainapp.py:1841
msgid "Upload To Radio"
msgstr "Scrivi su Radio"

#: ../chirp/ui/mainapp.py:1845
#, fuzzy
msgid "Import from data source"
msgstr "Importa da RFinder"

#: ../chirp/ui/mainapp.py:1847 ../chirp/ui/mainapp.py:1858
msgid "DMR-MARC Repeaters"
msgstr ""

#: ../chirp/ui/mainapp.py:1848 ../chirp/ui/mainapp.py:1859
msgid "RadioReference.com"
msgstr ""

#: ../chirp/ui/mainapp.py:1850 ../chirp/ui/mainapp.py:1861
msgid "RFinder"
msgstr ""

#: ../chirp/ui/mainapp.py:1851 ../chirp/ui/mainapp.py:1863
#, fuzzy
msgid "RepeaterBook"
msgstr "Importa da RepeaterBook"

#: ../chirp/ui/mainapp.py:1852 ../chirp/ui/mainapp.py:1864
msgid "RepeaterBook political query"
msgstr ""

#: ../chirp/ui/mainapp.py:1854 ../chirp/ui/mainapp.py:1866
msgid "RepeaterBook proximity query"
msgstr ""

#: ../chirp/ui/mainapp.py:1856 ../chirp/ui/mainapp.py:1862
msgid "przemienniki.net"
msgstr ""

#: ../chirp/ui/mainapp.py:1857
msgid "Query data source"
msgstr ""

#: ../chirp/ui/mainapp.py:1868
msgid "CHIRP Native File"
msgstr "File nativo CHIRP"

#: ../chirp/ui/mainapp.py:1870
msgid "CSV File"
msgstr "File CSV"

#: ../chirp/ui/mainapp.py:1871
msgid "Import from stock config"
msgstr "Importa da Configurazione Stock"

#: ../chirp/ui/mainapp.py:1873
msgid "Channel defaults"
msgstr ""

#: ../chirp/ui/mainapp.py:1876
msgid "Help"
msgstr "Aiuto"

#: ../chirp/ui/mainapp.py:1878
msgid "Get Help Online..."
msgstr ""

#: ../chirp/ui/mainapp.py:1889
#, fuzzy
msgid "Report Statistics"
msgstr "Report statistiche"

#: ../chirp/ui/mainapp.py:1891
msgid "Hide Unused Fields"
msgstr "Nascondi campi inutilizzati"

#: ../chirp/ui/mainapp.py:1893
#, fuzzy
msgid "Smart Tone Modes"
msgstr "Modalita' tono"

#: ../chirp/ui/mainapp.py:1895
msgid "Show Information"
msgstr ""

#: ../chirp/ui/mainapp.py:1897
msgid "Show Instructions"
msgstr ""

#: ../chirp/ui/mainapp.py:1899
msgid "Enable Developer Functions"
msgstr "Abilita Funzioni Sviluppatore"

#: ../chirp/ui/mainapp.py:2019
#, python-brace-format
msgid "A new version of CHIRP is available: {ver}. "
msgstr ""

#: ../chirp/ui/mainapp.py:2155
msgid "Error reporting is enabled"
msgstr "Rapporto errori abilitato"

#: ../chirp/ui/mainapp.py:2158
msgid ""
"If you wish to disable this feature you may do so in the <u>Help</u> menu"
msgstr "Se vuoi disabilitare questa opzione puoi farlo nel menu <u>Aiuto</u>"

#: ../chirp/ui/dstaredit.py:45
msgid "Callsign"
msgstr "Nominativo"

#: ../chirp/ui/dstaredit.py:131
msgid "Your callsign"
msgstr "Il tuo nominativo"

#: ../chirp/ui/dstaredit.py:139
msgid "Repeater callsign"
msgstr "Nominativo Ripetitore"

#: ../chirp/ui/dstaredit.py:147
msgid "My callsign"
msgstr "Il mio nominativo"

#: ../chirp/ui/dstaredit.py:185
msgid "Downloading MYCALL list"
msgstr "Scaricamento lista MYCALL"

#: ../chirp/ui/memdetail.py:244
msgid "General"
msgstr ""

#: ../chirp/ui/memdetail.py:286
msgid "Other"
msgstr ""

#: ../chirp/ui/memdetail.py:347
#, fuzzy
msgid "RX DTCS Code"
msgstr "DTCS Code"

#: ../chirp/ui/memdetail.py:351
#, fuzzy
msgid "Cross mode"
msgstr "Cross Mode"

#: ../chirp/ui/memdetail.py:411
msgid "Memory validation failed:"
msgstr ""

#: ../chirp/ui/editorset.py:85
#, python-format
msgid "Memories (%(variant)s)"
msgstr ""

#: ../chirp/ui/editorset.py:89
msgid "Memories"
msgstr "Memorie"

#: ../chirp/ui/editorset.py:100
msgid "D-STAR"
msgstr "D-STAR"

#: ../chirp/ui/editorset.py:149
msgid "Settings"
msgstr ""

#: ../chirp/ui/editorset.py:158
msgid "Browser"
msgstr ""

#: ../chirp/ui/editorset.py:275
#, python-brace-format
msgid "The {vendor} {model} has multiple independent sub-devices"
msgstr "Il {vendor} {model} ha sub-device multipli indipendenti"

#: ../chirp/ui/editorset.py:277
msgid "Choose one to import from:"
msgstr "Scegli da dove importare:"

#: ../chirp/ui/editorset.py:282
msgid "Cancelled"
msgstr "Annullato"

#: ../chirp/ui/editorset.py:287
msgid "Internal Error"
msgstr "Errore interno"

#: ../chirp/ui/editorset.py:328
#, python-brace-format
msgid ""
"There were errors while opening {file}. The affected memories will not be "
"importable!"
msgstr ""
"Ci sono stati degli errori nell'apertura del file {file}. Le memorie "
"interessate non potranno essere importate!"

#: ../chirp/ui/editorset.py:340
#, python-brace-format
msgid "There was an error during import: {error}"
msgstr "Errore durante l'apertura del file: {error}"

#: ../chirp/ui/editorset.py:348
msgid "Unsupported file type"
msgstr "Tipo file non supportato"

#: ../chirp/ui/editorset.py:364 ../chirp/ui/editorset.py:379
#, python-brace-format
msgid "There was an error during export: {error}"
msgstr "Errore durante l'esportazione del file: {error}"

#: ../chirp/ui/editorset.py:393
msgid "Priming memory"
msgstr "Preparazione memoria"

#: ../chirp/ui/common.py:246
msgid "Completed"
msgstr "Completato"

#: ../chirp/ui/common.py:247
msgid "idle"
msgstr "pronto"

#: ../chirp/ui/common.py:354
msgid "Details"
msgstr ""

#: ../chirp/ui/common.py:357
msgid "Proceed?"
msgstr ""

#: ../chirp/ui/common.py:366
#, fuzzy
msgid "Do not show this next time"
msgstr "Non mostrarlo di nuovo"

#: ../chirp/ui/bandplans.py:96
msgid ""
"Band plans define default channel settings for frequencies in a region.  "
"Choose a band plan or None for completely manual channel settings."
msgstr ""

#: ../chirp/ui/clone.py:40
#, python-brace-format
msgid "{vendor} {model} on {port}"
msgstr "{vendor} {model} su porta {port}"

#: ../chirp/ui/clone.py:112 ../chirp/ui/clone.py:113 ../chirp/ui/clone.py:173
msgid "Detect"
msgstr "Rileva"

#: ../chirp/ui/clone.py:136
msgid "Port"
msgstr "Porta"

#: ../chirp/ui/clone.py:137
msgid "Vendor"
msgstr "Produttore"

#: ../chirp/ui/clone.py:138
msgid "Model"
msgstr "Modello"

#: ../chirp/ui/clone.py:149
msgid "Radio"
msgstr "Radio"

#: ../chirp/ui/clone.py:178
#, python-brace-format
msgid "Unable to detect radio on {port}"
msgstr "Impossibile rilevare radio sulla porta {port}"

#: ../chirp/ui/clone.py:210
#, python-brace-format
msgid "Internal error: Unable to upload to {model}"
msgstr "Errore interno: impossibile caricare su {model}"

#: ../chirp/ui/clone.py:261
#, python-brace-format
msgid "Clone failed: {error}"
msgstr "Programmazione fallita: {error}"

#: ../chirp/ui/bankedit.py:58
#, fuzzy, python-format
msgid "Retrieving %s information"
msgstr "Recupero informazioni bank"

#: ../chirp/ui/bankedit.py:81
#, fuzzy, python-format
msgid "Setting name on %s"
msgstr "Scrittura nome del bank"

#: ../chirp/ui/bankedit.py:222
#, fuzzy, python-brace-format
msgid "Updating {type} index for memory {num}"
msgstr "Aggiornamento indice bank per la memoria {num}"

#: ../chirp/ui/bankedit.py:232
#, fuzzy, python-brace-format
msgid "Updating mapping information for memory {num}"
msgstr "Aggiornamento informazioni bank per la memoria {num}"

#: ../chirp/ui/bankedit.py:238 ../chirp/ui/bankedit.py:269
#, python-brace-format
msgid "Getting memory {num}"
msgstr "Acquisizione memoria {num}"

#: ../chirp/ui/bankedit.py:253
#, python-brace-format
msgid "Setting index for memory {num}"
msgstr "Scrittura indice per la memoria {num}"

#: ../chirp/ui/bankedit.py:262
#, fuzzy, python-brace-format
msgid "Getting {type} for memory {num}"
msgstr "Acquisizione bank per la memoria {num}"

#: ../chirp/ui/bankedit.py:284
msgid "Index"
msgstr "Indice"

#: ../chirp/ui/bankedit.py:375
#, fuzzy, python-brace-format
msgid "Getting {type} information for memory {num}"
msgstr "Acquisizione informazioni bank per la memoria {num}"

#: ../chirp/ui/bankedit.py:401
#, fuzzy, python-format
msgid "Getting %s information"
msgstr "Acquisizione informazioni bank"

#~ msgid "Bank"
#~ msgstr "Bank"

#~ msgid "With significant contributions by:"
#~ msgstr "Con il significativo contributo di:"

#~ msgid "Automatic Repeater Offset"
#~ msgstr "Offset Ripetitori Automatico"

#~ msgid "Bank Names"
#~ msgstr "Nomi Bank"

#~ msgid "Banks"
#~ msgstr "Bank"

#~ msgid "Delete all"
#~ msgstr "Elimina tutto"

#~ msgid "Delete (and shift up)"
#~ msgstr "Elimina (e sposta sopra)"

#~ msgid "Go"
#~ msgstr "Vai"

#~ msgid "%i errors during open, check the debug log for details"
#~ msgstr "%i errors during open, check the debug log for details"
