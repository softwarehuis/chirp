# Copyright 2008 Dan Smith <dsmith@danplanet.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import struct
from typing import Optional, Dict, Union, List


def hexprint(data: Union[List[bytes], bytes], addrfmt=None) -> str:
    """Return a hexdump-like encoding of @data"""
    if addrfmt is None:
        addrfmt = '%(addr)03i'

    block_size = 8

    lines = len(data) // block_size

    if isinstance(data, bytes):
        pad = bytes([0])
    else:
        pad = '\x00'

    if (len(data) % block_size) != 0:
        lines += 1
        data += pad * ((lines * block_size) - len(data))

    out = ""

    for block in range(0, (len(data) // block_size)):
        addr = block * block_size
        try:
            out += addrfmt % locals()
        except (OverflowError, ValueError, TypeError, KeyError):
            out += "%03i" % addr
        out += ': '

        left = len(data) - (block * block_size)
        if left < block_size:
            limit = left
        else:
            limit = block_size

        for j in range(0, limit):
            byte = data[(block * block_size) + j]
            if isinstance(byte, str):
                byte = ord(byte)
            out += "%02x " % byte

        out += "  "

        for j in range(0, limit):
            byte = data[(block * block_size) + j]
            if isinstance(byte, str):
                byte = ord(byte)
            if 0x20 < byte < 0x7E:
                out += "%s" % chr(byte)
            else:
                out += "."

        out += "\n"

    return out


def bcd_encode(val, bigendian: bool = True, width: Optional[int] = None) -> bytes:
    """This is really old and shouldn't be used anymore"""
    digits = []
    result = b""
    while val != 0:
        digits.append(val % 10)
        val /= 10

    if len(digits) % 2 != 0:
        digits.append(0)

    while width and width > len(digits):
        digits.append(0)

    for i in range(0, len(digits), 2):
        _val = struct.pack("B", (digits[i + 1] << 4) | digits[i])
        if bigendian:
            result = _val + result
        else:
            result = result + _val

    return result


def get_dict_rev(thedict: Dict, value):
    """Return the first matching key for a given @value in @dict"""
    _dict = {}
    for k, v in thedict.items():
        _dict[v] = k
    return _dict[value]


def safe_charset_string(indexes, charset, safe_char=" ") -> str:
    """Return a string from an array of charset indexes,
    replaces out of charset values with safechar"""
    assert safe_char in charset
    _string = ""
    for i in indexes:
        try:
            _string += charset[i]
        except IndexError:
            _string += safe_char
    return _string


class StringStruct(object):
    """String-compatible struct module."""

    @staticmethod
    def pack(*args) -> str:
        fmt = args[0]
        # Encode any string arguments to bytes
        _args = (bytes(x, 'utf-8') if isinstance(x, str)
                 else x
                 for x in args[1:])
        return struct.pack(fmt, *_args).decode('utf-8')

    @staticmethod
    def unpack(fmt, data):
        result = struct.unpack(fmt, bytes(data, 'utf-8'))
        # Decode any string results
        return tuple(x.decode('utf-8') if isinstance(x, bytes)
                     else x
                     for x in result)
