import contextlib
import logging
import serial as base_serial

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from chirp import bitwise

LOG = logging.getLogger('uicompat')


def SpinButton(adj):
    try:
        return Gtk.SpinButton(adj)
    except TypeError:
        sb = Gtk.SpinButton()
        sb.configure(adj, 1.0, 0)
        return sb


def Frame(label):
    try:
        return Gtk.Frame(label)
    except TypeError:
        f = Gtk.Frame()
        f.set_label(label)
        return f


class CompatSerial(base_serial.Serial):
    """A PY2-compatible Serial class

    This wraps serial.Serial to provide translation between
    hex-char-having unicode strings in radios and the bytes-wanting
    serial channel.

    This should only be used as a bridge for older drivers until
    they can be rewritten.
    """

    def write(self, data):
        data = bytes(data)
        return super(CompatSerial, self).write(data)

    def read(self, count):
        data = super(CompatSerial, self).read(count)
        return data.decode('utf-8')

    @classmethod
    def get(cls, *args, **kwargs):
        return cls(*args, **kwargs)


class CompatTooltips(object):
    def __init__(self):
        try:
            self.tips = Gtk.Tooltips()
        except AttributeError:
            self.tips = None

    def set_tip(self, widget, tip):
        if self.tips:
            self.tips.set_tip(widget, tip)
        else:
            widget.set_tooltip_text(tip)
